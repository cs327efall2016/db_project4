drop view contract_view ;
drop view paint_view ;
drop view tile_view ;

create view contract_view as
SELECT 
    contract_id,
    type,
    begin_date,
    end_date
FROM contract where type = 'contract' ;

create or replace TRIGGER contract_trigger
     INSTEAD OF insert ON contract_view
     FOR EACH ROW
BEGIN
    insert into contract(
        contract_id,
        type,
        begin_date,
        end_date)
    VALUES ( 
        :NEW.contract_id,
        'contract',
        :NEW.begin_date,
        :NEW.end_date) ;
END;
/

create view paint_view as
SELECT 
    contract_id,
    type,
    begin_date,
    end_date, paint_color 
FROM contract where type = 'paint' ;

create or replace TRIGGER paint_trigger
     INSTEAD OF insert ON paint_view
     FOR EACH ROW
BEGIN
    insert into contract(
        contract_id,
        type,
        begin_date,
        end_date, paint_color)
    VALUES ( 
        :NEW.contract_id,
        'paint',
        :NEW.begin_date,
        :NEW.end_date,
        :NEW.paint_color) ;
END;
/

create view tile_view as
SELECT 
    contract_id,
    type,
    begin_date,
    end_date, material
FROM contract where type = 'tile' ;

create or replace TRIGGER tile_trigger
     INSTEAD OF insert ON tile_view
     FOR EACH ROW
BEGIN
    insert into contract(
        contract_id,
        type,
        begin_date,
        end_date, material)
    VALUES ( 
        :NEW.contract_id,
        'tile',
        :NEW.begin_date,
        :NEW.end_date,
        :NEW.material) ;
END;
/
